from pybatfish.client.commands import *
from pybatfish.question.question import load_questions
from pybatfish.datamodel.flow import (HeaderConstraints,PathConstraints)
from pybatfish.question import bfq
import pandas as pd
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 2000)

#
def pause():
    if input("\n>>> Continue ? y(enter) or n : ") == "n" :
        print("   Terminated..\n")
        exit()
    return

def whatWeDo(q):
    print( "\n########################################################")
    print( "\n####             ", q )
    print( "\n########################################################")
    if input("\n>>> skip ? y(enter) or n : ") == "n" :
        return True

    return False


NETWORK_NAME = "network1"
BASE_SNAPSHOT_NAME = "snapshot1"
SNAPSHOT_PATH = "snapshot1"
BATFISH_SERVICE_IP = "localhost"

bf_session.host = BATFISH_SERVICE_IP
load_questions()

bf_set_network(NETWORK_NAME)
bf_init_snapshot(SNAPSHOT_PATH, name=BASE_SNAPSHOT_NAME, overwrite=True)

questions = [
  'initIssues()',
  'fileParseStatus()',
  'parseWarning()',
  'interfaceMtu()',
  'nodeProperties()',
  'switchedVlanProperties()',
  'interfaceProperties()',
  'switchedVlanProperties()',
  'ipOwners()',
  'layer3Edges()',
  'vxlanVniProperties()',
  'vxlanEdges()',
  'definedStructures()',
  'bgpProcessConfiguration()',
  'bgpEdges()',
  'bgpSessionCompatibility()',
  'bgpSessionStatus()',
]


for question in questions:
    if whatWeDo(question)is True:
        exec('print(  bfq.'+question+'.answer().frame()  )')
        pause()
