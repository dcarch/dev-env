# Requirements
 - Ansible Juniper.junos module (  ansible-galaxy install Juniper.junos )
 - junos-eznc python module ( sudo apt install python-pip, then, pip install junos-eznc )
 - jxmlease python module ( pip install jxmlease )
 - Add box "juniper/vqfx10k-re-18.4R" from image file: 10.251.83.132:/home/dca/image/vqfx-18.4R1.8-re-virtualbox
   For example, run (vagrant box add juniper/vqfx10k-re-18.4R  file:///home/dca/image/vqfx-18.4R1.8-re-virtualbox )
 - Add box "juniper/vqfx10k-pfe-18.4R" from image file: 10.251.83.132:/home/dca/image/vqfx-18.4R1.8-pfe-virtualbox)
   For example, run (vagrant box add juniper/vqfx10k-pfe-18.4R  file:///home/dca/image/vqfx-18.4R1.8-pfe-virtualbox )

# notice
 - Replace virtualbox v5 with https://download.virtualbox.org/virtualbox/6.0.12/virtualbox-6.0_6.0.12-133076~Ubuntu~bionic_amd64.deb

# Bring up and play
 1. Run ( vagrant up --no-provision )  to bring up boxes,  may need to run multiple times because vqfxpfe timeout when rebooting. This could take up to 40-50 minutes
 2. Run ( vagrant provision ) when 1. is successfully done
 3. Run ( ansible-playbook configuring_switch.yaml --extra-vars "some=switch") or (./doSwitch switch) to push switch configuration
 4. Run ( ansible-playbook configuring_server.yaml --extra-vars "some=server") or (./doServer server) to push server configure
 5. To change individual/group switch configuration use:
    ./doSwitch ss1 ,  or ./doSwitch leaf
 6. To change one or more server configuration use:
    ./doServer h1  ,  or ./doServer h2,h4
 7. To clean switch configuration run:
    ./cleanSW ss1 ,  or ./cleanSW spine
 8. Get into switches and servers. Inventory is defined as following in "Vagrantfile"
    ansible.groups = {
       "t2"           => ["ss1", "ss2"],
       "t1"           => ["s1", "s2","s3","s4"],
       "t0"           => ["l1", "l2","l3", "l4" ],
       "t0_border"    => ["bl1", "bl2" ],
       "server"       => ["h1", "h2","h3","h4","h5","h6" ],
       "leaf:children"    => ["t0", "t0_border" ],
       "spine:children"   => ["t2", "t1" ],
       "switch:children" => ["spine", "leaf" ],
       "all:children"      => ["switch", "server" ]
    }
    For example run ( vagrant ssh h1 ) to log into "h1" server
