# -*- mode: ruby -*-
# vi: set ft=ruby :

VAGRANTFILE_API_VERSION = "2"

## Generate a unique ID for this project
UUID = "DevEnvJunos"
EOS_MEM = "1280"
CU_MEM = "256"


## Define port mapping to build the Fabric
sspine_port_map = {  1 => [1,3,5,7],
                     2 => [2,4,6,8] }

spine_port_map  = {  1 => [1,2,    9,13,17],
                     2 => [3,4,   10,14,18],
                     3 => [5,6,   11,15,19],
                     4 => [7,8,   12,16,20]}

leaf_port_map  = {   1 => [9,10,  21,  24,30,34],
                     2 => [11,12, 21,  25,31,35],
                     3 => [13,14, 22,  26,32,36],
                     4 => [15,16, 22,  27,33,37]}

bleaf_port_map  = {  1 => [17,18, 23,  28],
                     2 => [19,20, 23,  29]}


server_port_map = {  1 => [24,25],
                     2 => [30,31],
                     3 => [26,27],
                     4 => [32,33]}

router_port_map  = {  1 => [28,29] } ## Connected with border leaf

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

    config.ssh.insert_key = false

    # Path used for routing engine console log.
    (File.exist?("/proc/version") and File.readlines("/proc/version").grep(/(Microsoft|WSL)/).size > 0) ? ( re_log = "NUL" ) : ( re_log = "/dev/null" )

    ##########################
    ## Super Spine     #######
    ##########################
    (1..2).each do |id|
        re_name  = ( "ss" + id.to_s ).to_sym

        ##########################
        ## Routing Engine  #######
        ##########################
        config.vm.define re_name do |cumulus|
            cumulus.vm.hostname = "ss#{id}"
            cumulus.vm.box = "CumulusCommunity/cumulus-vx"
            cumulus.vm.boot_timeout = 300

            # DO NOT REMOVE / NO VMtools installed
            cumulus.vm.synced_folder '.', '/vagrant', disabled: true

            # swp1-4
            (0..3).each do |seg_id|
                cumulus.vm.network 'private_network',
                    auto_config: false,
                    virtualbox__intnet: "#{UUID}_seg#{sspine_port_map[id][seg_id]}"
            end
            cumulus.vm.provider "virtualbox" do |v|
              v.customize ["modifyvm", :id, "--memory", "#{CU_MEM}"]
            end
        end
    end
    ##########################
    ##  Spine          #######
    ##########################
    (1..4).each do |id|
        re_name  = ( "s" + id.to_s ).to_sym

        ##########################
        ## Routing Engine  #######
        ##########################
        config.vm.define re_name do |cumulus|
            cumulus.vm.hostname = "s#{id}"
            cumulus.vm.box = "CumulusCommunity/cumulus-vx"
            cumulus.vm.boot_timeout = 900

            # VM can be really slow unless COM1 is connected to something.
            cumulus.vm.provider "virtualbox" do |v|
                v.customize ["modifyvm", :id, "--uartmode1", "file", re_log]
            end

            # DO NOT REMOVE / NO VMtools installed
            cumulus.vm.synced_folder '.', '/vagrant', disabled: true


            # em3 - em7 (xe-0/0/0 - xe-0/0/4)
            (0..4).each do |seg_id|
                cumulus.vm.network 'private_network',
                    auto_config: false,
                    virtualbox__intnet: "#{UUID}_seg#{spine_port_map[id][seg_id]}"
            end
            cumulus.vm.provider "virtualbox" do |v|
              v.customize ["modifyvm", :id, "--memory", "#{CU_MEM}"]
            end
        end
    end

    ##########################
    ## Leaf           #######
    ##########################
    (1..4).each do |id|
        veos_name  = ( "l" + id.to_s ).to_sym
        #########################################
        ## Arista vEOS External Router    #######
        #########################################
        config.vm.define veos_name do |veos|
            veos.vm.hostname = "l#{id}"
            # veos.vm.box = 'arista/veosbase'
            veos.vm.box = 'arista/eos4.21.2.2F'
            # veos.vm.base_mac = "0800274F99#{id}#{id}"
            # veos.ssh.username = "root"
            # veos.ssh.password = "vagrant"
            # veos.ssh.shell = "bash"

            # VM can be really slow unless COM1 is connected to something.
            (File.exist?("/proc/version") and File.readlines("/proc/version").grep(/(Microsoft|WSL)/).size > 0) ? ( re_log = "NUL" ) : ( re_log = "/dev/null" )
            veos.vm.provider "virtualbox" do |v|
                #v.customize ["modifyvm", :id, "--uartmode1", "file", re_log]
                # Display the VirtualBox GUI when booting the machine
                #v.gui = true
                v.customize ["modifyvm", :id, "--nic1", "nat", "--nictype1", "82540EM"]
                #v.customize ["modifyvm", :id, "--nic1", "hostonly", "--nictype1", "82540EM", "--hostonlyadapter1","vboxnet0"]

                v.customize ["modifyvm", :id, "--memory", "#{EOS_MEM}"]

                (2..7).each do |nicid|
                  v.customize ["modifyvm", :id, "--nic#{nicid}", "intnet", "--nictype#{nicid}", "82540EM","--nicpromisc#{nicid}","allow-all"]
                end
            end
            #
            # DO NOT REMOVE / NO VMtools installed
            veos.vm.synced_folder '.', '/vagrant', disabled: true
            #
            # # (eth1 -- eth7)
            # #note seg 1 goes to bl1, and seg2 goes to bl2
            (0..5).each do |seg_id|
                veos.vm.network 'private_network',
                  auto_config: false,
                  nic_type: "virtio",
                  virtualbox__intnet: "#{UUID}_seg#{leaf_port_map[id][seg_id]}"
            end
        end
    end

    ##########################
    ## Border Leaf     #######
    ##########################
    (1..2).each do |id|

        veos_name  = ( "bl" + id.to_s ).to_sym

        config.vm.define veos_name do |veos|
            veos.vm.hostname = "bl#{id}"
            veos.vm.box = 'arista/eos4.21.2.2F'
            # veos.vm.box = 'arista/veosbase'
            # veos.vm.base_mac = "0800274F98#{id}#{id}"
            # veos.ssh.username = "root"
            # veos.ssh.password = "vagrant"
            # veos.ssh.shell = "bash"

            (File.exist?("/proc/version") and File.readlines("/proc/version").grep(/(Microsoft|WSL)/).size > 0) ? ( re_log = "NUL" ) : ( re_log = "/dev/null" )
            veos.vm.provider "virtualbox" do |v|
                #v.customize ["modifyvm", :id, "--uartmode1", "file", re_log]
                # Display the VirtualBox GUI when booting the machine
                #v.gui = true
                v.customize ["modifyvm", :id, "--nic1", "nat", "--nictype1", "82540EM"]
                #v.customize ["modifyvm", :id, "--nic1", "hostonly", "--nictype1", "82540EM", "--hostonlyadapter1","vboxnet0"]

                v.customize ["modifyvm", :id, "--memory", "#{EOS_MEM}"]

                (2..7).each do |nicid|
                  v.customize ["modifyvm", :id, "--nic#{nicid}", "intnet", "--nictype#{nicid}", "82540EM","--nicpromisc#{nicid}","allow-all"]
                end
            end

            veos.vm.synced_folder '.', '/vagrant', disabled: true

            (0..3).each do |seg_id|
                veos.vm.network 'private_network',
                    auto_config: false,
                    nic_type: "virtio",
                    virtualbox__intnet: "#{UUID}_seg#{bleaf_port_map[id][seg_id]}"
            end
        end
    end


    ##########################
    ## Server          #######
    ##########################
    # (1..6).each do |id|
    #     srv_name = ( "h" + id.to_s ).to_sym
    #     config.vm.define srv_name do |srv|
    #         srv.vm.box = "robwc/minitrusty64"
    #         srv.vm.hostname = "h#{id}"
    #         srv.vm.network 'private_network', ip: "192.168.100.1#{id}", nic_type: '82540EM', virtualbox__intnet: "#{UUID}_seg#{server_port_map[id][0]}"
    #         srv.vm.network 'private_network', ip: "192.168.200.1#{id}", nic_type: '82540EM', virtualbox__intnet: "#{UUID}_seg#{server_port_map[id][1]}"
    #         srv.ssh.insert_key = true
    #         # srv.vm.provision "shell",
    #         #    inline: "sudo route add -net 10.0.10.0 netmask 255.255.255.0 gw 10.0.10.1"
    #     end
    # end
    (1..4).each do |id|
        srv_name = ( "h" + id.to_s ).to_sym
        config.vm.define srv_name do |srv|
            srv.vm.box = "CumulusCommunity/cumulus-vx"
            srv.vm.hostname = "h#{id}"
            srv.vm.network 'private_network', virtualbox__intnet: "#{UUID}_seg#{server_port_map[id][0]}", auto_config: false
            srv.vm.network 'private_network', virtualbox__intnet: "#{UUID}_seg#{server_port_map[id][1]}", auto_config: false
            srv.ssh.insert_key = true
            srv.vm.provider "virtualbox" do |v|
              v.customize ["modifyvm", :id, "--memory", "256"]
            end
        end
    end

    ##########################
    ## Router          #######
    ##########################
    (1..1).each do |id|
        rtr_name = ( "rt" + id.to_s ).to_sym
        config.vm.define rtr_name do |rtr|
            rtr.vm.box = "CumulusCommunity/cumulus-vx"
            rtr.vm.hostname = "rt#{id}"
            rtr.vm.network 'private_network', virtualbox__intnet: "#{UUID}_seg#{router_port_map[id][0]}", auto_config: false
            rtr.vm.network 'private_network', virtualbox__intnet: "#{UUID}_seg#{router_port_map[id][1]}", auto_config: false
            rtr.vm.network 'private_network', virtualbox__intnet: "#{UUID}_seg_dummy01", auto_config: false
            rtr.vm.provider "virtualbox" do |v|
              v.customize ["modifyvm", :id, "--memory", "#{CU_MEM}"]
            end
        end
    end



    #############################
    # Box provisioning    #######
    #############################
    config.vm.provision "ansible" do |ansible|
        ansible.compatibility_mode = "2.0"
        ansible.config_file = "ansible.cfg"
        ansible.groups = {
            "t2"           => ["ss1", "ss2"],
            "t1"           => ["s1", "s2","s3","s4"],
            "t0"           => ["l1", "l2","l3", "l4" ],
            "t0_border"    => ["bl1", "bl2" ],
            "server"       => ["h1", "h2","h3","h4","h5","h6" ],
            "router"       => ["rt1" ],
            "leaf:children"    => ["t0", "t0_border" ],
            "spine:children"   => ["t2", "t1" ],
            "switch:children" => ["spine", "leaf" ],
            "all:children"      => ["switch", "server" ]
        }
        ansible.playbook = "vagrant_initializing_device.yaml"
    end
end
