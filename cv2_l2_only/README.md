# Requirements
 - Install vagrant on host   ( on Mac-OS:  vagrant_2.2.5_x86_64.dmg)
 - Install virtualbox on host ( on Mac-OS:  VirtualBox-6.0.12-133076-OSX.dmg)
   Follow https://apple.stackexchange.com/questions/301303/virtualbox-5-1-28-fails-to-install-on-macos-10-13-due-to-kext-security to allow vagrant to control virtualbox
 - Install ansible
   On Mac: $ brew install ansible

 - Add vagrant box
   1) Cumulus box ( for T1/T2 )
      $vagrant box add CumulusCommunity/cumulus-vx   (choose virtualbox version)
   2) Arista EOS box ( for TOR leaf and border leaf)
      Download image "vEOS-lab-4.21.2.2F-virtualbox.box" to host
      (Can also be found at 10.251.83.132:/home/dca/image)
      $vagrant box add arista/eos4.21.2.2F  file:///Users/zyin000/image/vEOS-lab-4.21.2.2F-virtualbox.box

# Load and play
  - Change to cv2 folder "cd cv2", then run "vagrant up".
    It will take around 20 minutes to load up 12 switches, 1 router and 4 hosts.
  - Run "vagrant ssh 'device name'" to play
