from pybatfish.client.commands import *
from pybatfish.question.question import load_questions
from pybatfish.question.question import list_questions

from pybatfish.question import bfq

load_questions()

# Assign a friendly name to your network and snapshot
NETWORK_NAME = "example_network"
SNAPSHOT_NAME = "example_snapshot"

#SNAPSHOT_PATH = "/Users/syan200/devops/pybatfish/jupyter_notebooks/networks/example"

SNAPSHOT_PATH = "/Users/syan200/devops/junos/batfish"

# Now create the network and initialize the snapshot
bf_set_network(NETWORK_NAME)
bf_init_snapshot(SNAPSHOT_PATH, name=SNAPSHOT_NAME, overwrite=True)

# Ask a question and get a Pandas dataframe

list_questions()


answer = bfq.nodeProperties().answer().frame()

print(answer)

bgpBLStatus = bfq.bgpSessionStatus(nodes="/bl/").answer().frame()

print(bgpBLStatus)

bgpleafStatus = bfq.bgpSessionStatus(nodes="/^l/").answer().frame()

print(bgpleafStatus)


bgpsspineStatus = bfq.bgpSessionStatus(nodes="/^ss/").answer().frame()

print(bgpsspineStatus)

bgpspineStatus = bfq.bgpSessionStatus(nodes="/^s\d+/").answer().frame()

print(bgpspineStatus)

vlan_answer = bfq.switchedVlanProperties(nodes="/^l/").answer().frame()

print(vlan_answer)

vxlan_answer = bfq.vxlanVniProperties(nodes="/^l/").answer().frame()

print(vxlan_answer)

parse_status = bfq.fileParseStatus().answer().frame()

print(parse_status)
