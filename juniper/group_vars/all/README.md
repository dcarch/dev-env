# Varible defination  

#########################
# T0 Global
#########################
t0_domain:
  idx:   0 - 4                  # T0 domain
                                # 0 : domain for all none evpn T0 (Viper)
                                # 1 : xcloud (evpn)
                                # 2 : openstack  (evpn)
                                # 3 : legacy network,red/blue/green/black (evpn)

  evpn: true/false              # True: evpn overly ( Use case:  xcloud/openstack/legacy network)
                                # False: no overlay ( Use case:  Viper )

  type: private/public          # When evpn = true,
                                # private: dedicated borderleaf. Such as xcloud, openstack
                                # public:  shared borderleaf.  For red/blue/green/black

  color: red/blue/green/black   # Valid when type = public   

  vlans: [10,20,30,40,50,etc]   # Vlans defined in this domain  


#########################
# T0 leaf
#########################
idx:  0 - 369                 # Index of 370 workload + 8 border T0 switches

domain:  0 - 4                # Domain this leaf belongs to

function:   0/1               # 0: workload  1: border

vlans: [10,20,30, etc]        # vlans needed on this leaf

server_ports: [10,20,30,0,0,20,etc]  # vlan id assigned to each server port
